﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dynamics
{
    public class Album : ExpandableBase
    {
        public dynamic Performer { get; set; }
        public string Title { get; set; }

        public override string ToString()
        {
            return string.Format("{0}: {1}", Performer, Title);
        }
    }
}
