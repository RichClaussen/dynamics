﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;

using Dynamics.ViewModels;

namespace Dynamics
{
    public class DynamicObservableCollection<T> : ObservableCollection<dynamic>, ITypedList where T : BindableExpandoBase
    {
        public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
        {
            var properties = BindableExpandoBase.BindableTypes[typeof(T)];

            var propertyDescriptors = new List<PropertyDescriptor>();
            propertyDescriptors.AddRange(properties.Values);

            return new PropertyDescriptorCollection(propertyDescriptors.ToArray());
        }

        public string GetListName(PropertyDescriptor[] listAccessors)
        {
            return "master list";
        }
    }
}
