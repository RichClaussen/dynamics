﻿using System;
using System.Collections.ObjectModel;
using System.Windows.Input;

namespace Dynamics.ViewModels
{
    public class MainViewModel : ObservableItem
    {
        public DynamicObservableCollection<SongViewModel> Songs { get; private set; }

        public DynamicObservableCollection<OtherViewModel> Strings { get; private set; }

        private string performer;
        public string Performer
        {
            get { return performer; }
            set
            {
                performer = value;
                this.OnPropertyChanged(() => Performer);
            }
        }

        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                this.OnPropertyChanged(() => Title);
            }
        }

        public ICommand AddCommand { get; private set; }

        public MainViewModel()
        {
            AddCommand = new Command
                         {
                             CanExecuteDelegate = d => this.CanAddSong(),
                             ExecuteDelegate = d => this.AddSong(),
                         };

            Songs = new DynamicObservableCollection<SongViewModel>
                    {
                        new SongViewModel { Performer = "Journey", Title = "Don't Stop Believing", },
                        new SongViewModel { Performer = "Styx", Title = "Rockin' the Paradise", },
                        new SongViewModel { Performer = "Led Zepplin", Title = "Stairway to Heaven", },
                    };

            Songs[0].Length = new TimeSpan(0, 0, 4, 11);
            Songs[1].Length = new TimeSpan(0, 0, 3, 35);
            Songs[2].Length = new TimeSpan(0, 0, 8, 02);

            Songs[1].Composer = "Dennis DeYoung";

            Strings = new DynamicObservableCollection<OtherViewModel>
            {
                new OtherViewModel { Thing = "Hello World", },
                new OtherViewModel { Thing = "Is Cool?", },
            };

            Strings[0].FizzBin = 5;
            Strings[0].Muskrat = "Sweet Moses";
        }

        private bool CanAddSong()
        {
            return !string.IsNullOrWhiteSpace(this.Performer) && !string.IsNullOrWhiteSpace(this.Title);
        }

        private void AddSong()
        {
            Songs.Add(new SongViewModel { Performer = this.Performer, Title = this.Title, });
            Performer = null;
            Title = null;
        }
    }
}
